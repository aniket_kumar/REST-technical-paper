# REpresentational State Transfer

>REST is an architectural style for providing standard between computer system and web, making system communication easier. The implementation of server and client are developed independently. This means code from client can be changed without affecting server and vise versa.

![Rest Architecture](https://miro.medium.com/max/2400/1*EbBD6IXvf3o-YegUvRB_IA.jpeg)

## Verbs

There are 4 verbs in HTTP request :-

* GET : Retrieve some resources.
* POST : Create some resource.
* PUT : Update some details
* DELETE : Remove some details

## Accepted Parameters

* image -- image/png , image/jpeg ,image/gif
* audio -- audio/wav , audio/mpeg
* video -- video/mp4 , video/ogg
* application -- application/json , application/pdf , application/xml

## Response Codes

* 200(OK) - This is for successful HTTP request.
* 201(CREATED) - This is for  HTTP request that resulted in an item being successfully created.
* 204(NO CONTENT) - This is for for successful HTTP requests, where nothing is being returned in the response body.
* 400(BAD REQUEST) - This is for bad request syntax, excessive size, or another client error.
* 403(FORBIDDEN) - The client does not have permission to access this resource.
* 404(NOT FOUND) - This is for Resource not found. 

## Models

* Client-server
* Stateless
* Cacheable
* Uniform interface
* Layered system

### 1.Client-Server

> In client-server user interface is been separated form data storage to increase portability.The app doesn't need to bother about how the server stores data,the particular database used,etc.The point of intersection between the server and the app is the database schema. the database schema contains a description of the data stores and the layout in which it is stored.

![clientServer](https://miro.medium.com/max/468/1*_Bc3-FKtaV--gfh2QftNrg.png)

### 2.Stateless

>System that follows REST paradigm are stateless,which means server and client doesn't know any thing about each other. This makes REST application achieve reliability,better performance,scalability.

![stateless](https://miro.medium.com/max/475/1*Ztyoi5oUwujgoRpBqP3KDw.png)

### 3.Cacheable

>Cache are added to improve the network efficiency nd to increase user-perceived performance by reducing the average latency of a series of interactions.The server specifies it using a set of cache control directives in the HTTP header. Maximum age of the cache is one of the cache control directives. This is also known as time-to-live.If we have ever run a ping command in our terminal you have probably seen a bunch of TTL responses. That is the time-to-live for that response packet. In this case, the time-to-live is calculated as the maximum number of IP routers that the packet can pass through. After that, the packet will be expired and thrown away.

### 4. Uniform interface

>The uniform interface constraint is fundamental to the design of any REST system. It simplifies and decouples the architecture, which enables each part to evolve independently.

### 5. Layered system

> In layered architecture, Each layer only knows about the next layer. Each layer is responsible for specific role and it knows how to do it,and passes information to and from its immediate neighboring layer to get the job done.

## References

1. [Wikipedia](https://en.wikipedia.org/wiki/Representational_state_transfer)

2. [Medium](https://en.wikipedia.org/wiki/Representational_state_transfer)